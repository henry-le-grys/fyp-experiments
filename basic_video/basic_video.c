#include <gst/gst.h>

typedef struct {
    GstElement* video;
    GstElement* audio;
} AppSinks;

static void pad_added_handler(GstElement* src, GstPad* pad, AppSinks* sinks);

int main(int argc, char *argv[]) {
  GstElement* pipeline;
  GstMessage* msg;
  GstStateChangeReturn ret;

  /* Initialize GStreamer */
  gst_init(&argc, &argv);

  /* Create the elements */
  GstElement* source = gst_element_factory_make ("filesrc", "source");
  GstElement* filter = gst_element_factory_make("decodebin", "decoder");
  GstElement* sink = gst_element_factory_make("autovideosink", "video-sink");

  GstElement* audio_convert = gst_element_factory_make("audioconvert", "audio-conv");
  GstElement* audio_resample = gst_element_factory_make("audioresample", "audio-resample");
  GstElement* audio_sink = gst_element_factory_make("autoaudiosink", "audio-sink");

  AppSinks userdata = {
      .video = sink,
      .audio = audio_convert,
  };

  /* Create the empty pipeline */
  pipeline = gst_pipeline_new("test-pipeline");

  if (!pipeline || !source || !sink || !filter) {
    g_printerr("Not all elements could be created.\n");
    return -1;
  }

  /* Build the pipeline */
  gst_bin_add_many(GST_BIN (pipeline), source, filter, sink, audio_convert, audio_resample, audio_sink, NULL);
  if (gst_element_link(source, filter) != TRUE) {
    g_printerr("Elements could not be linked.\n");
    gst_object_unref(pipeline);
    return -1;
  }

  if (gst_element_link_many(audio_convert, audio_resample, audio_sink, NULL) != TRUE) {
      g_printerr("Audio elements could not be linked.\n");
      gst_object_unref(pipeline);
      return -1;
  }

  g_signal_connect(filter, "pad-added", G_CALLBACK(pad_added_handler), &userdata);

  /* Modify the source's properties */
  g_object_set(source, "location", "/home/henry/videos/wolfalice.mp4", NULL);

  /* Start playing */
  ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);
  if (ret == GST_STATE_CHANGE_FAILURE) {
    g_printerr("Unable to set the pipeline to the playing state.\n");
    gst_object_unref(pipeline);
    return -1;
  }

  /* Wait until error or EOS */
  GstBus* bus = gst_element_get_bus(pipeline);
  msg = gst_bus_timed_pop_filtered(bus, GST_CLOCK_TIME_NONE, GST_MESSAGE_ERROR | GST_MESSAGE_EOS);

  /* Parse message */
  if (msg != NULL) {
    GError *err;
    gchar *debug_info;

    switch (GST_MESSAGE_TYPE(msg)) {
      case GST_MESSAGE_ERROR:
        gst_message_parse_error(msg, &err, &debug_info);
        g_printerr("Error received from element %s: %s\n", GST_OBJECT_NAME (msg->src), err->message);
        g_printerr("Debugging information: %s\n", debug_info ? debug_info : "none");
        g_clear_error(&err);
        g_free(debug_info);
        break;
      case GST_MESSAGE_EOS:
        g_print("End-Of-Stream reached.\n");
        break;
      default:
        /* We should not reach here because we only asked for ERRORs and EOS */
        g_printerr("Unexpected message received.\n");
        break;
    }
    gst_message_unref(msg);
  }

  /* Free resources */
  gst_object_unref(bus);
  gst_element_set_state(pipeline, GST_STATE_NULL);
  gst_object_unref(pipeline);
  return 0;
}

static void pad_added_handler(GstElement* src, GstPad* pad, AppSinks* sinks) {
    g_print("Received new pad '%s' from '%s'\n", GST_PAD_NAME(pad), GST_ELEMENT_NAME(src));

    GstCaps* pad_caps = gst_pad_get_current_caps(pad);
    GstStructure* pad_struct = gst_caps_get_structure(pad_caps, 0);
    const gchar* pad_type = gst_structure_get_name(pad_struct);
    GstElement* selected_sink;

    g_print("Pad type: %s\n", pad_type);

    if (g_str_has_prefix(pad_type, "video/x-raw")) {
        selected_sink = sinks->video;
    } else {
        selected_sink = sinks->audio;
    }

    GstPad* sink_pad = gst_element_get_static_pad(selected_sink, "sink");
    GstPadLinkReturn ret = gst_pad_link(pad, sink_pad);
    if (GST_PAD_LINK_FAILED(ret)) {
        g_printerr("Link failed.\n");
    }

    gst_object_unref(sink_pad);
}
